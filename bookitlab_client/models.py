"""Models used by the client."""

import json
import typing
from datetime import date
from enum import Enum
from typing import Any, Dict, List, Optional

import pydantic
from pydantic import Field

NameType = typing.TypeVar("NameType")
BaseModel_T = typing.TypeVar("BaseModel_T", bound="BaseModel")


def to_lower_camel(s: str) -> str:
    """Alias generator to avoid breaking PEP8."""
    first, *others = s.split("_")
    return "".join([first.lower(), *map(str.capitalize, others)])


class BaseModel(pydantic.BaseModel):
    """Expanded BaseModel for convenience."""

    @classmethod
    def from_dict(cls: typing.Type[BaseModel_T], data: Dict[Any, Any]) -> BaseModel_T:
        """Initialize class from dict."""
        return cls(**data)  # noqa

    @classmethod
    def from_json(cls: typing.Type[BaseModel_T], json_data: str) -> BaseModel_T:
        """Initialize class from json file."""
        data = json.loads(json_data)
        return cls.from_dict(data)

    def dict(  # noqa: A003
        self,
        by_alias: bool = True,
        exclude_unset: bool = True,
        *args: str,
        **kwargs: Any,
    ) -> Dict[Any, Any]:  # noqa: A003
        """Export model to dict.

        Sets by_alias and exclude_unset for convenience.

        by_alias ensures that the alias_generator is used for field names, and
        exclude_unset excludes optional fields that were not explicitly set.
        """
        return super().dict(
            by_alias=by_alias,
            exclude_unset=exclude_unset,
            *args,
            **kwargs,
        )


class Budget(BaseModel):
    """
    Budget model.

    When bookitlab refers to "Budget" or "Project" this corresponds to the
    "Delprosjekt" (eng: sub project) level in Unit4.
    # required fields project_name and active_from
    """

    id: Optional[int]  # noqa: A003
    project_name: str
    project_group: Optional[str]
    active_from: Optional[date]
    active_to: Optional[date]
    project_leader: Optional[str]
    owner_costcenter: Optional[str]
    project_number: Optional[str]


class Status(str, Enum):
    """Allowed status values."""

    Success = "Success"
    Failure = "Failure"


class InvoiceStatus(BaseModel):
    """Model for invoice status."""

    invoice_id: int
    reference: str


class StatusMessageRequest(BaseModel):
    """Model for status updates.

    To be used in requests to the /invoices/batches/{batchId} endpoint.
    """

    status: Optional[Status] = Field(
        None, description='Transactions acceptance status - "Success" or "Failure"'
    )
    invoices: Optional[List[InvoiceStatus]] = Field(
        None, description="Invoice reference mapping"
    )
    messages: Optional[List[str]] = Field(None)


class StatusSalesorderRequest(BaseModel):
    """Model for status updates.

    To be used in requests to the /invoices/batches/order_status endpoint.
    """

    status: str
    salesorder_number: Optional[int]
    invoice_number: int
