"""Client library for use with BookitLabs API."""

from .client import BookitlabClient, get_client
from .version import get_distribution


__all__ = ["BookitlabClient", "get_client"]
__version__ = get_distribution().version
