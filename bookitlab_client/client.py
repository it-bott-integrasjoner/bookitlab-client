"""Client for connecting to Bookitlab API."""
import json
import logging
import urllib.parse
from typing import Any, Dict, List, Optional, Type, Union, overload

import requests

from .models import BaseModel_T, Budget, StatusMessageRequest, StatusSalesorderRequest

logger = logging.getLogger(__name__)

JsonType = Any


def merge_dicts(*dicts: Optional[Dict[str, str]]) -> Dict[Any, Any]:
    """
    Combine a series of dicts without mutating any of them.

    >>> merge_dicts({'a': 1}, {'b': 2})
    {'a': 1, 'b': 2}
    >>> merge_dicts({'a': 1}, {'a': 2})
    {'a': 2}
    >>> merge_dicts(None, None, None)
    {}
    """
    combined = dict()
    for d in dicts:
        if not d:
            continue
        for k in d:
            combined[k] = d[k]
    return combined


def _urljoin(
    baseurl: str, endpoint: str, primary_key: Union[str, int, None] = None
) -> str:
    """Fix basic mistakes in urls and join."""
    if isinstance(primary_key, int):
        primary_key = str(primary_key)
    elif primary_key is None:
        primary_key = ""
    elif not isinstance(primary_key, str):
        raise TypeError(
            f"Invalid primary key. Expected str, int or None. Got {type(primary_key)}"
        )

    return "/".join(
        (
            baseurl.rstrip("/"),
            endpoint.lstrip("/").rstrip("/"),
            primary_key.rstrip("/"),
        )
    ).rstrip("/")


class BookitlabEndpoints:
    """Class for keeping track of endpoints."""

    def __init__(
        self,
        url: str,
        budgets_url: Optional[str] = None,
        status_url: Optional[str] = None,
        status_salesorder_url: Optional[str] = None,
    ) -> None:
        """Get endpoints relative to the Bookitlab API URL."""
        self.baseurl = url
        if budgets_url is None:
            budgets_url = "/budgets"  # use default if None
        if status_url is None:
            status_url = "/invoices/batches"  # use default if None
        if status_salesorder_url is None:
            # use default if None
            status_salesorder_url = "/invoices/batches/order_status"
        self.budgets_url = budgets_url
        self.status_url = status_url
        self.status_salesorder_url = status_salesorder_url

    def __repr__(self) -> str:
        """Return nice representation."""
        return f"{type(self).__name__}({self.baseurl!r})"

    def get_budgets(self) -> str:
        """Get budgets from endpoint."""
        return _urljoin(self.baseurl, self.budgets_url)

    def put_budget(self, budget: Union[int, Budget]) -> str:
        """URL for budgets endpoint."""
        budget_id = budget.id if isinstance(budget, Budget) else budget
        if budget_id is None:
            raise ValueError("budget must have id set when you do a PUT")
        return _urljoin(self.baseurl, self.budgets_url, budget_id)

    @property
    def post_budget(self) -> str:
        """URL for budgets endpoint."""
        return _urljoin(self.baseurl, self.budgets_url)

    def post_status(self, batch_id: str) -> str:
        """Url for status update endpoint."""
        return _urljoin(self.baseurl, self.status_url, batch_id)

    def post_status_salesorder(self) -> str:
        """Url for the endpoint for updating status of salesorder."""
        return _urljoin(self.baseurl, self.status_salesorder_url)


class BookitlabClient(object):
    """Bookitlab API client."""

    default_headers = {
        "Accept": "application/json",
    }

    def __init__(
        self,
        url: str,
        headers: Union[None, Dict[str, str]] = None,
        return_objects: bool = True,
        use_sessions: bool = True,
    ):
        """
        Intialize method.

        :param str url: Base API URL
        :param dict headers: Append extra headers to all requests
        :param bool return_objects: Return objects instead of raw JSON
        :param bool use_sessions: Keep HTTP connections alive (default True)
        """
        self.urls = BookitlabEndpoints(url)
        self.headers = merge_dicts(self.default_headers, headers)
        self.return_objects = return_objects
        if use_sessions:
            self.session = requests.Session()
        else:
            self.session = requests  # type: ignore

    def _build_request_headers(
        self, headers: Optional[Dict[str, str]]
    ) -> Dict[str, Any]:
        request_headers = {}
        for h in self.headers:
            request_headers[h] = self.headers[h]
        if headers:
            for h in headers:
                request_headers[h] = headers[h]
        return request_headers

    def call(
        self,
        method_name: str,
        url: str,
        headers: Optional[Dict[str, Any]] = None,
        params: Union[None, Dict[str, Any], Any] = None,
        return_response: bool = True,
        **kwargs: Any,
    ) -> JsonType:
        """Make sending requests easier."""
        headers = self._build_request_headers(headers)
        if params is None:
            params = {}
        logger.debug(
            "Calling %s %s with params=%r",
            method_name,
            urllib.parse.urlparse(url).path,
            params,
        )
        r = self.session.request(
            method_name,
            url,
            headers=headers,
            params=params,
            **kwargs,
        )
        if r.status_code in (500, 400, 401):
            logger.warning("Got HTTP %d: %r", r.status_code, r.content)
        if return_response:
            return r
        r.raise_for_status()
        try:
            return r.json()
        except ValueError:
            return r.text

    def get(self, url: str, **kwargs: Any) -> JsonType:
        """Send GET request."""
        return self.call("GET", url, **kwargs)

    def put(self, url: str, **kwargs: Any) -> JsonType:
        """Send PUT request."""
        return self.call("PUT", url, **kwargs)

    def post(self, url: str, **kwargs: Any) -> JsonType:
        """Send POST request."""
        return self.call("POST", url, **kwargs)

    @overload
    def object_or_data(  # noqa: D102
        self, cls: Type[BaseModel_T], data: List[Dict[str, Any]]
    ) -> Union[List[BaseModel_T], List[Dict[str, Any]]]:
        ...

    @overload
    def object_or_data(  # noqa: D102
        self, cls: Type[BaseModel_T], data: Dict[str, Any]
    ) -> Union[BaseModel_T, Dict[str, Any]]:
        ...

    def object_or_data(
        self,
        cls: Type[BaseModel_T],
        data: Union[BaseModel_T, Dict[str, Any], List[Any]],
    ) -> Union[BaseModel_T, List[Any], Dict[str, Any]]:
        """Return model or data.

        Meant for use in the return statement of an API method so that methods match the
        expected behaviour according to self.return_objects.
        """
        if not self.return_objects:
            return data
        if isinstance(data, dict):
            return cls.from_dict(data)
        if isinstance(data, list):
            return [cls.from_dict(d) for d in data]
        raise TypeError(f"Expected list or dict, got {type(data)}")

    def _list_or_data(
        self, cls: Type[BaseModel_T], data: Union[List[Any], Any]
    ) -> Union[List[Any], Any]:
        if not self.return_objects:
            return data

        if isinstance(data, list):
            return [cls(**prop) for prop in data]
        return [cls(**data)]

    def get_budgets(
        self, only_active: bool = True, only_online: bool = True
    ) -> Union[List[Budget], List[Dict[Any, Any]], None]:  # noqa
        """Get budgets, default only active and online are retrieved.

        When active only active and online only imported projects from finance system
        (unit4) are returned.
        """
        url = self.urls.get_budgets()
        params = {}
        if only_active:
            params["active"] = only_active
        if only_online:
            params["online"] = only_online

        response = self.get(url, params=params)

        if response.status_code == 404:
            return None
        if response.status_code == 200:
            data = response.json()
            return self._list_or_data(Budget, data)
        return response.raise_for_status()

    def _put_or_post_budget(
        self, budget: Budget, put: bool
    ) -> List[Union[Budget, Dict[str, Any]]]:
        """Send PUT or POST request to budget endpoint."""
        url = self.urls.put_budget(budget) if put else self.urls.post_budget
        headers = {"content-type": "application/json"}
        # TODO: id is readonly, should it be excluded?
        json = budget.json()
        method = self.put if put else self.post
        response = method(url=url, data=json, headers=headers)
        if response.status_code == 200:
            return self.object_or_data(Budget, response.json())

        response.raise_for_status()
        return []

    def put_budget(self, budget: Budget) -> List[Union[Budget, Dict[str, Any]]]:
        """Send PUT request to budget endpoint."""
        return self._put_or_post_budget(budget=budget, put=True)

    def post_budget(self, budget: Budget) -> List[Union[Budget, Dict[str, Any]]]:
        """Send POST request to budget endpoint."""
        return self._put_or_post_budget(budget=budget, put=False)

    def post_status(
        self, request_message: StatusMessageRequest, batch_id: str
    ) -> JsonType:
        """Send POST request to status endpoint."""
        url = self.urls.post_status(batch_id)
        headers = {"content-type": "application/json"}
        response = self.post(
            url, headers=headers, data=request_message.json(exclude_unset=True)
        )
        response.raise_for_status()
        return response

    def post_status_salesorder(
        self, request_message: StatusSalesorderRequest
    ) -> JsonType:
        """Send POST request to status endpoint."""
        url = self.urls.post_status_salesorder()
        headers = {"content-type": "application/json"}
        json_obj = json.dumps(request_message.__dict__)
        response = self.post(url, headers=headers, data=json_obj)
        response.raise_for_status()
        return response


def get_client(config_dict: Dict[str, Any]) -> BookitlabClient:
    """Get a BookitlabClient from configuration."""
    return BookitlabClient(**config_dict)
