# Contributing to bookitlab-client

## The contribution process at a glance

1. Read the [README.md file](README.md).
2. Set up your environment to be able to [run all tests](README.md#running-the-tests).  They must pass
3. Prepare your changes:
    * Make sure your tests pass cleanly on [mypy](README.md#type-check), [bandit](https://bandit.readthedocs.io/), and [flake8](https://flake8.pycqa.org/)
    * [Format your code](#code-style) with `black`
    * Create a merge request

## Code style

### Black

This project uses [black](https://github.com/psf/black/) to format the code.
See [black documentation](https://black.readthedocs.io/en/stable/editor_integration.html) for how to set up _black_ with your editor.

To autoformat all code, run

```shell script
git ls-files '*.py' | xargs black -l 88
```

[Flake8](https://flake8.pycqa.org/en/latest/) is also used as a linter.

Code style errors will result in CI pipeline failing.

You can use a `pre-commit` hook to have git run linters before commit.
Save this to `.git/hooks/pre-commit` and make it executable.

```sh
#!/bin/sh

tox -e linters
```
