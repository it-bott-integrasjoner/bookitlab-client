# bookitlab-client

[![pipeline status](https://git.app.uib.no/it-bott-integrasjoner/bookitlab-client/badges/master/pipeline.svg)](https://git.app.uib.no/it-bott-integrasjoner/bookitlab-client/-/commits/master)
[![coverage report](https://git.app.uib.no/it-bott-integrasjoner/bookitlab-client/badges/master/coverage.svg)](https://git.app.uib.no/it-bott-integrasjoner/bookitlab-client/-/commits/master)

Client for doing HTTP requests to the bookitlab api

## Set up development environment

### Virtual environment

#### Create

```bash
virtualenv --python=python3 venv
```

#### Activate

```bash
source venv/bin/activate
```

### Install dependencies

```bash
pip install -r requirements.txt -r requirements-test.txt
```

This should install all dependencies, including development dependencies (code formatter, linters etc).

## Running the tests

### Run all tests

Use [tox](https://tox.readthedocs.io/) to run all tests and linters.

```bash
tox
```

### Run pytest

```bash
python -m pytest
```

### Type check

We use static type checker [mypy](http://mypy-lang.org/).

```bash
python -m mypy -p bookitlab_client
```


## Example

```python
from bookitlab_client import BookitlabClient
from bookitlab_client.models import PlaceHolder

client = BookitlabClient(
    url="https://example.com",
    headers={'X-Gravitee-API-Key': 'c-d-a-b'}
)


ph = PlaceHolder.from_dict({"placeholder": "foo"})

response = client.get_placeholder1("foo")
```
