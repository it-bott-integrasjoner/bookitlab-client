import json
import pytest
from unittest.mock import MagicMock
from typing import Dict

from requests import HTTPError

from bookitlab_client.client import BookitlabClient, _urljoin
from bookitlab_client.models import (
    Budget,
    StatusMessageRequest,
    StatusSalesorderRequest,
)
from tests.conftest import load_json_file


@pytest.fixture
def header_name():
    return "X-Test"


@pytest.fixture
def client_cls(header_name):
    class TestClient(BookitlabClient):
        default_headers = {
            header_name: "6a9a32f0-7322-4ef3-bbce-6685a3388e67",
        }

    return TestClient


def test_get_budgets(client_cls, baseurl, requests_mock):
    headers: Dict[str, str] = {}
    client = client_cls(baseurl, headers=headers)

    budgets = load_json_file("budgets.json")
    url = "https://localhost/budgets?active=True&online=True"
    requests_mock.get(
        url,
        json=budgets,
        status_code=200,
    )

    budgets = client.get_budgets()
    assert len(budgets) == 2


def test_get_budgets_query(client_cls, baseurl, budget_list, requests_mock):
    headers = {}  # type: ignore[var-annotated]
    client = client_cls(baseurl, headers=headers)

    url = "https://localhost/budgets"
    requests_mock.get(
        url,
        json=budget_list,
        status_code=200,
    )

    budgets = client.get_budgets(only_active=False, only_online=False)
    assert len(budgets) == 3


def test_init_does_not_mutate_arg(client_cls, baseurl):
    headers = {}  # type: ignore[var-annotated]
    client = client_cls(baseurl, headers=headers)
    assert headers is not client.headers
    assert not headers


def test_init_applies_default_headers(client_cls, baseurl, header_name):
    headers = {}  # type: ignore[var-annotated]
    client = client_cls(baseurl, headers=headers)
    assert header_name in client.headers
    assert client.headers[header_name] == client.default_headers[header_name]


def test_init_modify_defaults(client_cls, baseurl, header_name):
    headers = {header_name: "ede37fdd-a2ae-4a96-9d80-110528425ea6"}
    client = client_cls(baseurl, headers=headers)
    # Check that we respect the headers arg, and don't use default_headers
    assert client.headers[header_name] == headers[header_name]
    # Check that we don't do this by mutating default_headers
    assert client.default_headers[header_name] != headers[header_name]


def test_header_replacement(client_with_a_header, baseurl, requests_mock):
    """Given a BookitlabClient created with a "global" header,
    making a request with the same header key,
    should replace the "global" header value"""
    requests_mock.get(
        baseurl,
        text="some content",
        status_code=200,
        headers={"content-type": "replaced"},
    )

    response = client_with_a_header.call(
        method_name="GET", url=baseurl, headers={"content-type": "replaced"}
    )
    assert response.status_code == 200
    assert response.text == "some content"
    assert response.headers == {"content-type": "replaced"}


def test_header_replacement2(client_with_a_header, requests_mock, baseurl):
    """Given a BookitlabClient created with a "global" header,
    making a request with the same header key,
    should replace the "global" header value,
    while new headers should be added to the request"""
    requests_mock.get(
        baseurl,
        text="some content",
        status_code=200,
        headers={"content-type": "replaced", "key": "val"},
    )

    response = client_with_a_header.call(
        method_name="GET", url=baseurl, headers={"content-type": "replaced"}
    )
    assert response.status_code == 200
    assert response.text == "some content"
    assert response.headers == {"content-type": "replaced", "key": "val"}


def test_endpoints_put_budget_int(budget, client):
    budget_id = budget.get("id")
    assert client.urls.put_budget(budget_id) == f"https://localhost/budgets/{budget_id}"


def test_endpoints_put_budget_object(budget, client):
    budget = Budget.from_dict(budget)
    assert client.urls.put_budget(budget) == f"https://localhost/budgets/{budget.id}"


def test_endpoints_post_budget(client):
    assert client.urls.post_budget == f"https://localhost/budgets"


def test__urljoin_primary_key_str():
    assert (
        _urljoin("http://example.com", "endpoint", "id")
        == "http://example.com/endpoint/id"
    )


def test__urljoin_primary_key_int():
    assert (
        _urljoin("http://example.com", "endpoint", 1) == "http://example.com/endpoint/1"
    )


def test__urljoin_primary_key_none():
    assert _urljoin("http://example.com", "endpoint") == "http://example.com/endpoint"


def test__urljoin_primary_key_invalid():
    with pytest.raises(TypeError):
        _urljoin("http://example.com", "endpoint", {""})  # type: ignore[arg-type]


def test__urljoin_excessive_slashes():
    assert (
        _urljoin("http://example.com///", "///endpoint////", "id//")
        == "http://example.com/endpoint/id"
    )


def test_object_or_data_dict_in_object_out(client, budget):
    client.return_objects = True

    assert Budget.from_dict(budget) == client.object_or_data(Budget, budget)


def test_object_or_data_dict_in_dict_out(client, budget):
    client.return_objects = False

    assert budget == client.object_or_data(Budget, budget)


def test_object_or_data_dicts_in_objects_out(client, budget_list):
    client.return_objects = True

    assert [Budget.from_dict(x) for x in budget_list] == client.object_or_data(
        Budget, budget_list
    )


def test_object_or_data_dicts_in_dicts_out(client, budget_list):
    client.return_objects = False

    assert budget_list == client.object_or_data(Budget, budget_list)


def test_put_budget_200(client, budget, requests_mock, baseurl):
    # Setup
    obj = Budget.from_dict(budget)
    requests_mock.put(
        client.urls.put_budget(obj),
        text=json.dumps([budget]),
        status_code=200,
    )
    # Wrap methods for counting
    put = MagicMock(wraps=client.put)
    client.put = put

    # Execute
    updated = client.put_budget(obj)
    assert updated == [obj]
    assert put.call_count == 1


def test_put_budget_fails_with_id_not_set(client, budget, requests_mock, baseurl):
    # Setup
    budget_id = budget.get("id")
    del budget["id"]
    obj = Budget(**budget)
    requests_mock.put(
        client.urls.put_budget(budget_id),
        text=json.dumps([budget]),
        status_code=200,
    )

    # Execute
    with pytest.raises(ValueError):
        client.put_budget(obj)


def test_put_budget_raise_on_500(client, budget, requests_mock):
    # Setup
    obj = Budget.from_dict(budget)

    requests_mock.put(
        client.urls.put_budget(obj),
        status_code=500,
    )

    # Execute
    with pytest.raises(HTTPError):
        client.put_budget(obj)


def test_post_budget_200(client, budget, requests_mock, baseurl):
    # Setup
    obj = Budget.from_dict(budget)
    requests_mock.post(
        client.urls.post_budget,
        text=json.dumps([budget]),
        status_code=200,
    )
    # Wrap methods for counting
    post = MagicMock(wraps=client.post)
    client.post = post

    # Execute
    updated = client.post_budget(obj)
    assert updated == [obj]
    assert post.call_count == 1


def test_post_budget_raise_on_500(client, budget, requests_mock, baseurl):
    # Setup
    obj = Budget.from_dict(budget)
    requests_mock.post(
        client.urls.post_budget,
        status_code=500,
    )

    # Execute
    with pytest.raises(HTTPError):
        client.post_budget(obj)


def test_post_status_ok(client, request_message, requests_mock, baseurl):
    # Setup
    requests_mock.post(baseurl + "/invoices/batches/123", status_code=200)
    # Execute
    model = StatusMessageRequest(**request_message)
    response = client.post_status(model, 123)
    # Ensure POST request matches expectation
    assert response.status_code == 200
    assert requests_mock.request_history[-1].json() == request_message


def test_post_status_salesorder_ok(
    client, request_message_salesorder, requests_mock, baseurl
):
    # Setup
    requests_mock.post(baseurl + "/invoices/batches/order_status", status_code=200)
    # Execute
    model = StatusSalesorderRequest(**request_message_salesorder)
    response = client.post_status_salesorder(model)
    # Ensure POST request matches expectation
    assert response.status_code == 200  # Returns empty 200


def test_post_status_bad_request(client, request_message, requests_mock, baseurl):
    # Setup
    requests_mock.post(baseurl + "/invoices/batches/123", status_code=400)
    # Execute
    model = StatusMessageRequest(**request_message)
    with pytest.raises(
        HTTPError,
        match="400 Client Error: None for url: https://localhost/invoices/batches/123",
    ):
        client.post_status(model, 123)


def test_post_status_forbidden(client, request_message, requests_mock, baseurl):
    # Setup
    requests_mock.post(baseurl + "/invoices/batches/123", status_code=403)
    # Execute
    model = StatusMessageRequest(**request_message)
    with pytest.raises(
        HTTPError,
        match="403 Client Error: None for url: https://localhost/invoices/batches/123",
    ):
        client.post_status(model, 123)


def test_post_status_notfound(client, request_message, requests_mock, baseurl):
    # Setup
    requests_mock.post(baseurl + "/invoices/batches/123", status_code=404)
    # Execute
    model = StatusMessageRequest(**request_message)
    with pytest.raises(
        HTTPError,
        match="404 Client Error: None for url: https://localhost/invoices/batches/123",
    ):
        client.post_status(model, 123)
