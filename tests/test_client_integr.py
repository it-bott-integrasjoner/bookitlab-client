import pytest
import yaml

from bookitlab_client.client import BookitlabClient
from bookitlab_client.models import Budget


def get_config(filename=None):
    with open(filename, "r") as f:
        content = f.read()
        return yaml.load(content, Loader=yaml.FullLoader)


@pytest.fixture
def client():
    config = get_config("config.yml")["bookitlab_client"]
    return BookitlabClient(**config)


@pytest.mark.integration
def test_get_budgets(client):
    """Get budgets wich are active and imported from unit4"""
    budgets = client.get_budgets()
    assert len(budgets) > 0
    for x in budgets:
        assert type(x) == Budget


@pytest.mark.integration
def test_get_budgets_return_objects(client):
    client.return_objects = False
    budgets = client.get_budgets()
    assert len(budgets) > 0
    for x in budgets:
        assert type(x) == dict


# Skipping this test since this fails in bookitab, bad test data with active_from =""
# which should not occur
@pytest.mark.integration
def xtest_get_budgets_only_active(client):
    """Get all budgets from bookitlab also budgets that are not from unit4"""
    budgets = client.get_budgets(only_active=True, only_online=False)
    assert len(budgets) > 0
    for x in budgets:
        assert type(x) == Budget
