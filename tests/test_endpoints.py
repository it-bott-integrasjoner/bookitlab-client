import typing

from bookitlab_client.client import BookitlabEndpoints


def test_init(baseurl):
    endpoints = BookitlabEndpoints(baseurl)
    assert endpoints.baseurl == baseurl
    assert endpoints.budgets_url == "/budgets"
    assert endpoints.status_url == "/invoices/batches"
    assert endpoints.get_budgets() == "https://localhost/budgets"
    assert endpoints.post_status("17") == "https://localhost/invoices/batches/17"


def test_init_args(baseurl):
    endpoints = BookitlabEndpoints(baseurl, "foo/", "bar/baz")

    # Ensure url is built properly with all slash combinations
    assert endpoints.budgets_url == "foo/"
    assert endpoints.get_budgets() == "https://localhost/foo"
    endpoints.budgets_url = "/foo/"
    assert endpoints.get_budgets() == "https://localhost/foo"
    endpoints.budgets_url = "foo"
    assert endpoints.get_budgets() == "https://localhost/foo"
    endpoints.budgets_url = "/foo"
    assert endpoints.get_budgets() == "https://localhost/foo"

    assert endpoints.status_url == "bar/baz"
    assert endpoints.post_status("17") == "https://localhost/bar/baz/17"
    endpoints.status_url = "/bar/baz"
    assert endpoints.post_status("17") == "https://localhost/bar/baz/17"
    endpoints.status_url = "bar/baz/"
    assert endpoints.post_status("17") == "https://localhost/bar/baz/17"
    endpoints.status_url = "/bar/baz/"
    assert endpoints.post_status("17") == "https://localhost/bar/baz/17"


def test_init_none(baseurl):
    endpoints = BookitlabEndpoints(baseurl, None, None)
    assert endpoints.baseurl == baseurl
    assert endpoints.budgets_url == "/budgets"
    assert endpoints.status_url == "/invoices/batches"
    assert endpoints.get_budgets() == "https://localhost/budgets"
    assert endpoints.post_status("17") == "https://localhost/invoices/batches/17"


def test_init_from_pydantic_model(baseurl):
    """
    Check that default values are used when explicitly giving None for endpoint urls
    through a pydantic model
    """
    from pydantic import BaseModel

    class Config(BaseModel):
        url: str
        budgets_url: typing.Optional[str]
        status_url: typing.Optional[str]

    config = Config(url=baseurl)
    endpoints = BookitlabEndpoints(**config.dict())

    assert endpoints.budgets_url == "/budgets"
    assert endpoints.status_url == "/invoices/batches"
