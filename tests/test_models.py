import unittest
from pydantic import parse_obj_as
from datetime import datetime
from bookitlab_client.models import Budget
from tests.conftest import load_json_file


class TestModels(unittest.TestCase):
    def test_budget_from_dict(self):
        budget_json = load_json_file("budget.json")
        x = Budget.from_dict(budget_json)
        for name, field in x.__fields__.items():
            assert parse_obj_as(
                field.type_, budget_json.get(name)
            ) == x.__getattribute__(name)

    def test_budget_model(self):
        """Test case for budget model"""

        as_json = load_json_file("budget.json")
        budget = Budget.from_dict(as_json)

        self.assert_budget(budget)

    def test_budget_to_dict_budget_model(self):
        """Test case for budget model dict"""

        as_json = load_json_file("budget.json")
        budget = Budget.from_dict(as_json)
        self.assertEqual(1, budget.id)

        # When
        budget_dict = budget.dict()

        # Then
        self.assertEqual(1, budget_dict["id"])
        self.assert_budget(Budget(**budget_dict))

    def assert_budget(self, budget):
        self.assertEqual(1, budget.id)
        self.assertEqual("100", budget.project_number)
        self.assertEqual("budget 1", budget.project_name)
        self.assertEqual("group 1", budget.project_group)
        self.assertEqual(
            datetime.strptime("2020-03-20", "%Y-%m-%d").date(), budget.active_from
        )
        self.assertEqual(
            datetime.strptime("9999-12-31", "%Y-%m-%d").date(), budget.active_to
        )
        self.assertEqual("leader_username", budget.project_leader)
        self.assertEqual("kost senter", budget.owner_costcenter)
