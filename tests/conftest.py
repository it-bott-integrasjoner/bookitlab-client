import json
import os

import pytest

from bookitlab_client import BookitlabClient


def load_json_file(name):
    """Load json file from the fixtures directory"""
    here = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__).rsplit("/", 1)[0])
    )
    with open(os.path.join(here, "tests/fixtures", name)) as f:
        data = json.load(f)
    return data


@pytest.fixture
def baseurl():
    return "https://localhost"


@pytest.fixture
def client(baseurl):
    return BookitlabClient(baseurl)


@pytest.fixture()
def client_no_objects(baseurl):
    return BookitlabClient(baseurl, return_objects=False)


@pytest.fixture
def client_with_a_header(baseurl):
    return BookitlabClient(baseurl, {"content-type": "test"})


@pytest.fixture
def budget():
    return {
        "id": -270085157,
        "project_name": "The Yellow Meads of Asphodel",
        "project_group": "East of Eden",
        "active_from": "2021-02-17",
        "active_to": "2021-03-01",
        "project_leader": "Ring of Bright Water",
        "owner_costcenter": "Dying of the Light",
        "project_number": "The Cricket on the Hearth",
    }


@pytest.fixture()
def budget_list():
    return [
        {
            "id": 920376650,
            "project_name": "Absalom, Absalom!",
            "project_group": "A Catskill Eagle",
            "active_from": "2020-12-09",
            "active_to": "2020-12-09",
            "project_leader": "The Golden Apples of the Sun",
            "owner_costcenter": "Consider Phlebas",
            "project_number": "The Needle's Eye",
        },
        {
            "id": -1166333691,
            "project_name": "Those Barren Leaves, Thrones, Dominations",
            "project_group": "Down to a Sunless Sea",
            "active_from": "2021-02-15",
            "active_to": "2021-01-09",
            "project_leader": "Blue Remembered Earth",
            "owner_costcenter": "The Moon by Night",
            "project_number": "Little Hands Clapping",
        },
        {
            "id": -984662685,
            "project_name": "The Last Temptation",
            "project_group": "The Skull Beneath the Skin",
            "active_from": "2020-12-30",
            "active_to": "2021-03-08",
            "project_leader": "The Mirror Crack'd from Side to Side",
            "owner_costcenter": "O Pioneers!",
            "project_number": "Nectar in a Sieve",
        },
    ]


def pytest_collection_modifyitems(config, items):
    if config.option.keyword or config.option.markexpr:
        return
    skip_integration = pytest.mark.skip(
        reason='Not running with pytest -m "integration"'
    )
    for item in items:
        if "integration" in item.keywords:
            item.add_marker(skip_integration)


@pytest.fixture
def request_message():
    return load_json_file("request_message.json")


@pytest.fixture
def request_message_salesorder():
    return load_json_file("request_message_salesorder.json")
